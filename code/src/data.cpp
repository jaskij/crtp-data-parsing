#include <data.hpp>

#include <data_impl.hpp>

data::data(uint8_t version) :
    m_version(version)
{ }

std::shared_ptr<data> data::parse(const std::array<uint8_t, 512> device_data)
{
    switch (device_data[0]) {
    case 0:
        return std::shared_ptr<data>(new data_v0(device_data));
    case 1:
        return std::shared_ptr<data>(new data_v1(device_data));
    case 2:
        return std::shared_ptr<data>(new data_v2(device_data));
    }

    throw std::runtime_error("data::parse_data encountered unknown data version");
}
