#pragma once

#include <cstdint>

template<uint8_t V>
struct data_traits
{ };

template<>
struct data_traits<0>
{
    static constexpr bool has_a = true;
    static constexpr bool has_b = true;
    static constexpr bool has_mapping = false;
};

template<>
struct data_traits<1>
{
    static constexpr bool has_a = true;
    static constexpr bool has_b = false;
    static constexpr bool has_mapping = true;
};

template<>
struct data_traits<2>
{
    static constexpr bool has_a = false;
    static constexpr bool has_b = true;
    static constexpr bool has_mapping = true;
};
