#pragma once

#include <array>
#include <cstdint>
#include <memory>

struct point
{
    uint16_t x,y;
    bool operator==(const point& other) const {return x == other.x && y == other.y;}
};

struct line
{
    point start, end;
    bool operator==(const line& other) const {return start == other.start && end == other.end;}
};

class data
{
public:
    data(uint8_t version);

    virtual ~data() = default;

    constexpr uint8_t version() const {return m_version;}

    virtual double a() const = 0;
    virtual double b() const = 0;
    virtual line mapping() const = 0;

    static std::shared_ptr<data> parse(const std::array<uint8_t, 512> device_data);

private:
    const uint8_t m_version;
};

