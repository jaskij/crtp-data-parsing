#pragma once

#include "data.hpp"
#include "data_traits.hpp"

template<uint8_t V>
class data_impl : public data
{
public:
    data_impl(const std::array<uint8_t, 512>& data_from_device);

    double a() const override;
    double b() const override;
    line mapping() const override;

private:
    using traits = data_traits<V>;
    using storage_type = std::array<uint8_t, 511>;

    const std::array<uint8_t, 511> m_data;

    static std::array<uint8_t, 511> convert_data(const std::array<uint8_t, 512>& data_from_device);

    static constexpr auto a_addr = 0;
    static constexpr auto b_addr = 8;
    static constexpr auto mapping_addr = 16;
};

//////////////////////////////////////
//
// helper stuff - aliases for specific templates
// and parsing function
//
//////////////////////////////////////

using data_v0 = data_impl<0>;
using data_v1 = data_impl<1>;
using data_v2 = data_impl<2>;

//////////////////////////////////////
//
// actual template implementation
//
//////////////////////////////////////

template<uint8_t V>
data_impl<V>::data_impl(const std::array<uint8_t, 512>& data_from_device) :
    data(data_from_device[0]),
    m_data(convert_data(data_from_device))
{
    if(version() != V) {
        throw std::logic_error("Using data_impl to parse wrong data version");
    }
}

template<uint8_t V>
std::array<uint8_t, 511> data_impl<V>::convert_data(const std::array<uint8_t, 512>& data_from_device)
{
    std::array<uint8_t, 511> data;
    std::copy(std::next(data_from_device.cbegin()), data_from_device.cend(), data.begin());
    return data;
}

template<uint8_t V>
double data_impl<V>::a() const
{
    if(traits::has_a) {
        return *reinterpret_cast<const double*>(&m_data[a_addr]);
    }

    return 3.0;
}

template<uint8_t V>
double data_impl<V>::b() const
{
    if(traits::has_b) {
        return *reinterpret_cast<const double*>(&m_data[b_addr]);
    }

    return 4.0;
}

template<uint8_t V>
line data_impl<V>::mapping() const
{
    if(traits::has_mapping) {
        return *reinterpret_cast<const line*>(&m_data[mapping_addr]);
    }

    return line();
}
