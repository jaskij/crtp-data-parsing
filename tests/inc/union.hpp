#pragma once

#include <array>
#include <cstdint>

#include <data.hpp>

struct __attribute__((packed)) data_struct
{
    uint8_t version;
    double a;
    double b;
    line mapping;
};

union data_union
{
    std::array<uint8_t, 512> array;

    data_struct s;
};
