#include <gtest/gtest.h>

#include <data_impl.hpp>
#include <union.hpp>

TEST(Parsing, TestParseV0)
{
    data_union u;
    u.s.version = 0;
    u.s.a = 5;
    u.s.b = 19;
    u.s.mapping = {{1,1,}, {3,3}};

    auto d = data::parse(u.array);

    EXPECT_FLOAT_EQ(u.s.a, d->a());
    EXPECT_FLOAT_EQ(u.s.b, d->b());
    EXPECT_EQ(line(), d->mapping());
}

TEST(Parsing, TestParseV1)
{
    data_union u;
    u.s.version = 1;
    u.s.a = 5;
    u.s.b = 19;
    u.s.mapping = {{1,1,}, {3,3}};

    auto d = data::parse(u.array);

    EXPECT_FLOAT_EQ(u.s.a, d->a());
    EXPECT_FLOAT_EQ(4.0, d->b());
    EXPECT_EQ(u.s.mapping, d->mapping());
}

TEST(Parsing, TestParseV2)
{
    data_union u;
    u.s.version = 2;
    u.s.a = 5;
    u.s.b = 19;
    u.s.mapping = {{1,1,}, {3,3}};

    auto d = data::parse(u.array);

    EXPECT_FLOAT_EQ(3.0, d->a());
    EXPECT_FLOAT_EQ(u.s.b, d->b());
    EXPECT_EQ(u.s.mapping, d->mapping());
}

TEST(Parsing, TestErrors)
{
    data_union u;
    u.s.version = 3;
    EXPECT_THROW(data::parse(u.array), std::runtime_error);
    EXPECT_THROW(data_v0(u.array), std::logic_error);
}

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
