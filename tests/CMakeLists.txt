enable_testing()
find_package(GTest REQUIRED)
include(GoogleTest)

add_library(union INTERFACE)
target_include_directories(union INTERFACE inc)
target_sources(union INTERFACE inc/union.hpp)
target_link_libraries(union INTERFACE data_parser)

add_executable(test_parse src/test_parse.cpp)
target_link_libraries(test_parse PRIVATE union data_parser GTest::GTest GTest::Main)
gtest_add_tests(TARGET test_parse)
