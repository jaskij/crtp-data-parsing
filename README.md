# CRTP data parsing

This is a quick project to demonstrate usage of quasi-[CRTP](https://en.wikipedia.org/wiki/Curiously_recurring_template_pattern)
in data parsing for a [Software Engineering StackExchange question](https://softwareengineering.stackexchange.com/questions/399541/how-to-best-unify-different-data-formats)
